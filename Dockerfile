FROM nextcloud:latest

RUN apt-get update && apt-get install -y libmagickcore-6.q16-3-extra libbz2-dev && rm -rf /var/lib/apt/lists/* && docker-php-ext-install bz2
