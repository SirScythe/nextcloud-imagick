Nextcloud library image with libmagickcore-6.q16-3-extra added.

New images are built automatically by GitLab Pipelines and pushed to https://hub.docker.com/r/sirscythe/nextcloud-imagick
